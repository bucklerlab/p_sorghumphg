method_names=(CONSENSUS_mxDiv0_newtest CONSENSUS_mxDiv001_newtest CONSENSUS_mxDiv0001_newtest CONSENSUS_mxDiv00001_newtest CONSENSUS_mxDiv00005_newtest CONSENSUS_mxDiv000005_newtest CONSENSUS_mxDiv00025_newtest CONSENSUS_mxDiv0000025_newtest CONSENSUS_mxDiv00075_newtest CONSENSUS_mxDiv000075_newtest)
config_file="config.txt"


for method in "${method_names[@]}"
do
echo ${method}

./tassel-5-standalone/run_pipeline.pl -Xmx100g -debug \
-HaplotypeGraphBuilderPlugin -configFile ${config_file} \
-methods ${method} -includeVariantContexts true -endPlugin \
-ReferenceRangeSummaryPlugin -outputFile ChibasDB_${method}.txt \
-endPlugin

done
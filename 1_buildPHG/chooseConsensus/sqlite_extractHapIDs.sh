#!/bin/sh

taxaList=(00-SB-FSDT427 BF-95-11-195 C10-68-2 C36-52-1 CIR1-0G2-4G-1G-M-M CIR-2-7-3G-N-M-A27 D1-370-DB-020 D1-371-DA-088 D20-422-DB-074 D29-381-DA-091 D30-391-DA-003 D30-395-DB-054 D33-404-DB-007 D36-398-DB-083 D38-413-DB-095 D46-417-DB-022 D47-362-DA-094 D48-382-DB-055 D9-331-DA-029 DEKABES Papesek PCR-2-23C-1-M-1 SANTO-275 Wiley)
database="SbicolorPHG_CHIBAS_05-04-2019_398taxa.db"
db_methods=(CONSENSUS_mxDiv001,5 CONSENSUS_mxDiv00005,6)
target_dir="/workdir/sej65/PHG_400taxa/path_files/"

for taxon in $taxaList
do
	echo ${taxon}
	for method in db_methods:
	do
		KEY=${method%,*}
		VAL=${i#*,}
		echo $KEY " " $VAL

		mkdir ${target_dir}${KEY}

		sqlite3 ${database} <<END_SQL
		.mode csv
		.output ${target_dir}${KEY}/${KEY}_${taxon}_path.txt
		select haplotypes_id from haplotypes hap, gamete_haplotypes gh, gametes gam, genotypes geno where hap.gamete_grp_id=gh.gamete_grp_id and gh.gameteid=gam.gameteid and gam.genoid=geno.genoid and hap.method_id=${VAL} and geno.line_name='${taxon}';
END_SQL
done
done

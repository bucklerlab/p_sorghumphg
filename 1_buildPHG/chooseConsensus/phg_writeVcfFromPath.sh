#!/bin/bash

config="/workdir/sej65/LoadRefDataDocker/config.txt"
snp_position_file="/workdir/sej65/PHG_24taxa/ChibasFounders_gbs_unfiltered_taxaNames.recode.vcf"
reference="/workdir/sej65/Reference/Sbicolor_313_v3.0_numberedChr.fa"
cons_level=(0 001 0001 00001 0005 00005 00025 000025 00075 000075)

for consensus in "${cons_level[@]}"
do
echo ${consensus}

time ./tassel-5-standalone/run_pipeline.pl -debug -Xmx120g -HaplotypeGraphBuilderPlugin -configFile ${config} \
-methods CONSENSUS_mxDiv${consensus}_newtest,refRegionGroup -includeVariantContexts true -endPlugin \
-ImportHaplotypePathFilePlugin -inputFileDirectory ./path_files/${consensus}/ -endPlugin \
-PathsToVCFPlugin -positionVCF ${snp_position_file} -outputAllSNPs false -ref ${reference} \
-outputFile DiscordanceTest_mxDiv${consensus}_24taxa.vcf -endPlugin > 8-11_retest_discordance.txt

done
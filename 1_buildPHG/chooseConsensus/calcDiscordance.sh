consensus_levels=(mxDiv0 mxDiv001 mxDiv0001 mxDiv00001 mxDiv0005 mxDiv00005 mxDiv00025 mxDiv000025 mxDiv00075 mxDiv000075)
trustedSNPs="ChibasFounders_mergedVCFs_diploid_hetsToMissing_rmIndels_mac3_maxMissing1_anchorsOnly_thin100kb_sitesInHapMap.recode.vcf.gz"

for consensus in "${consensus_levels[@]}"
do

echo

vcftools --gzvcf ${trustedSNPs} \
--gzdiff ./consensus_vcfs/CONSENSUS_${consensus}_24taxa.vcf.gz \
--diff-indv-discordance \
--out ./consensus_discordance/${consensus}_WGSdiscordance

done
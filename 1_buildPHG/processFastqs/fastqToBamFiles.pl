#!/usr/local/bin/perl

## This script works if your are working remotely with CBSU machines.

# Adjust the two variables below:
$INDEX="/workdir/sej65/ref_idx/Sbicolor_313_v3.0_numberedChr.fa"; # provide genome bwa index prefix (with path)
$threads=22;  # number of threads

# If files are named *.fq instead of *.fastq, also change lines 10, 19, and 20
$cmd = 'ls *.fastq >fastqList.txt';  ## this has to run through fastq files directory
system($cmd);

open (FIL, "fastqList.txt");
while (<FIL>) {
    chomp;
    if($_ =~ /_R1.fastq/) {
        @tmp = split (/_/, $_); #Files are named in the SM_ID1_ID2_otherinfo_1.fq.gz or SM_ID1_ID2_otherinfo_2.fq.gz
        
        $forwardFile = $tmp[0]."_".$tmp[1]."_".$tmp[2]."_R1.fastq";
        $reverseFile = $tmp[0]."_".$tmp[1]."_".$tmp[2]."_R2.fastq";
        
        #Sample name
        $sample = "$tmp[0]";
        $sam = $sample.".sam";
        $t = localtime;

        #Set Read Group
        $sm = "$tmp[0]";
        $rg = "$tmp[1]";
        $lb = "lib1";
        $pl = "ILLUMINA";
        
        print "\n\nProcessing fastq files for the sample -  $sample \n";
        print "Reading\n", "\t$forwardFile\n", "\t$reverseFile\n";
        
        # Aligning with BWA-MEM
	    $unSortedBAM = "$sample"."_unsorted.bam";
        $cmd = "bwa mem -t $threads -R '\@RG\\tID:$rg\\tSM:$sm\\tLB:$lb\\tPL:$pl' ";
        $cmd .= " $INDEX $forwardFile $reverseFile > $sam";
        print("\t\t$cmd\n");
        system($cmd);
        
        print "\tFinished alignement of $sample at $t\n";
        
        ## Generating unsorted BAM file
        $unSortedBAM = "$sample"."_unsorted.bam";
        $cmd = "samtools view -bS $sam > ./$unSortedBAM";
        print "\tConverting SAM to unsorted BAM - started at $t\n";
        print("\t\t$cmd\n");
        system($cmd);
        
        print "\tFinished generating unsorted BAM file at $t\n";
        print "\tSorting BAM file started at $t\n";
        
        # Generating sorted BAM file
        $bamFile = $sample.".bam";
        $cmd = "samtools sort -@ $threads -m 4G $unSortedBAM -o $bamFile";
        print("\t\t$cmd\n");
        system($cmd);
        
        print "\tFinished at $t\n";
        
        # removing fastq and unsortedBAM files
        #$cmd = "rm $sam";
        #print("\t$cmd\n");
        #system($cmd);
        
        $cmd = "rm $unSortedBAM";
        print("\t$cmd\n");
        system($cmd);

	#$cmd = "rm $forwardFile";
	#print("\t$cmd\n");
	#system($cmd);

	#$cmd = "rm $reverseFile";
	#print("\t$cmd\n");
	#system($cmd);
        
        # indexing BAM file
        $cmd = "samtools index $bamFile";
        print("\t\t$cmd\n");
        system($cmd);
        print "Finished BAM file generation of the sample - $sample\n";
    }
}

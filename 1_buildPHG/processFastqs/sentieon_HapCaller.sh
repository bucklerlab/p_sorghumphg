#!/bin/bash -x

license=cbsulogin2.tc.cornell.edu:8990
export SENTIEON_LICENSE=$license
RELEASE=/programs/sentieon-genomics-201704.01
SENTIEON='/programs/sentieon-genomics-201704.01/bin/sentieon'
NUMBER_THREADS=23
REFERENCE='/workdir/sej65/Reference'
ploidy=2 #Change ploidy to 1 if creating GVCFs for the PHG, keep at 2 if creating GVCFs for Beagle imputation

mkdir /workdir/sej65/BEAGLE_imputation_files/repeat_SNPcalling/sentieon_out
HaplotyperOutputDir='/workdir/sej65/BEAGLE_imputation_files/repeat_SNPcalling/sentieon_out'

taxaNames="`ls *.bam | cut -d. -f1 | sort -u`"

for taxon in $taxaNames
do

echo "Taxon: " $taxon

time $SENTIEON driver \
	-t $NUMBER_THREADS \
	-i ${taxon}.bam \
	--algo LocusCollector \
	--fun score_info ${taxon}_SCORE.txt \

time $SENTIEON driver \
	-t $NUMBER_THREADS \
	-i ${taxon}.bam \
	--algo Dedup \
	--rmdup \
	--score_info ${taxon}_SCORE.txt \
	--metrics ${taxon}_dedup_metrics \
	${taxon}_dedup.bam

time $SENTIEON driver \
	-t $NUMBER_THREADS \
	-r $REFERENCE/Sbicolor_313_v3.0_numberedChr.fa \
	-i ${taxon}_dedup.bam \
	--algo QualCal \
	${taxon}_recal_data.table

time $SENTIEON driver \
	-t $NUMBER_THREADS \
	-r $REFERENCE/Sbicolor_313_v3.0_numberedChr.fa \
	-i ${taxon}_dedup.bam \
	-q ${taxon}_recal_data.table \
	--algo QualCal \
	${taxon}_recal_data.table.post

time $SENTIEON driver \
	-t $NUMBER_THREADS \
	--algo QualCal \
	--plot \
	--before ${taxon}_recal_data.table \
	--after ${taxon}_recal_data.table.post \
	${taxon}_recal_result.csv

time $SENTIEON driver plot QualCal \
	${taxon}_recal_data.table.post \
	${taxon}_recal_result.csv

time $SENTIEON driver \
	-t $NUMBER_THREADS \
	-r $REFERENCE/Sbicolor_313_v3.0_numberedChr.fa \
	-i ${taxon}_dedup.bam \
	-q ${taxon}_recal_data.table \
	--algo Haplotyper \
	--ploidy ${ploidy} \
    --emit_mode all \
    ${HaplotyperOutputDir}/${taxon}_WGS_haplotype_caller_output.vcf

done


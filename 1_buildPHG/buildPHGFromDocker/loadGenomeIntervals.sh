## LoadGenomeIntervals script
# Adds the reference genome and reference ranges to the PHG. 
# Script can be found at https://bitbucket.org/bucklerlab/practicalhaplotypegraph/wiki/Home

# Change the mount points to reflect file locations on your local machine. 
# Do not change the mount point for the docker (after :/)

config_file="config.txt"
reference="Sbicolor_313_v3.0_numberedChr.fa"
reference_ranges="Sbicolor_conserved_gene_intervals_extended_sorted_merged500.bed"
genome_data="SbicolorRef_BTx623_load_data.txt"
create_db="true"

docker1 run --name cbsu_phg_container --rm \
	-v /path/to/output/:/tempFileDir/outputDir/ \
	-v /path/to/reference/:/tempFileDir/data/reference/ \
	-v ./p_sorghumphg/1_buildPHG/buildPHGFromDocker/input_files/:/tempFileDir/data/ \
	-v ./p_sorghumphg/1_buildPHG/buildPHGFromDocker/input_files/:/tempFileDir/answer/ \
	-t maizegenetics/phg:0.0.9 \
	/LoadGenomeIntervals.sh ${config_file} ${reference} ${reference_ranges} ${genome_data} ${create_new_db}
## createConsensus script
# Condenses haplotypes in the PHG to a set of consensus haplotypes. 
# Script can be found at https://bitbucket.org/bucklerlab/practicalhaplotypegraph/wiki/Home

# Change the mount points to reflect file locations on your local machine. 
# Do not change the mount point for the docker (after :/)

config_file="config.txt"
reference="Sbicolor_313_v3.0_numberedChr.fa"
add_haplotypes_method="GATK_PIPELINE"
consensus_method="CONSENSUS_mxDiv00025"


docker1 run --name cbsu_phg_container_consensus --rm \
        -v /path/to/reference/:/tempFileDir/data/reference/ \
        -v /path/to/db/SbicolorPHG_CHIBAS_05-04-2019_25taxa.db:/tempFileDir/outputDir/SbicolorPHG_CHIBAS_05-04-2019_25taxa.db \
        -v ./1_buildPHG/buildPHGFromDocker/input_files/${config_file}:/tempFileDir/data/config.txt \
        -t maizegenetics/phg:0.0.9 \
        /CreateConsensi.sh /tempFileDir/data/${config_file} ${reference} ${add_haplotypes_method} ${consensus_method}
## createHaplotypes script
# Adds the taxa in all reference ranges to the PHG.
# Script can be found at https://bitbucket.org/bucklerlab/practicalhaplotypegraph/wiki/Home

# Change the mount points to reflect file locations on your local machine.
# Do not change the mount point for the docker (after :/)

taxonList=(00-SB-FSDT427 BF-95-11-195 C10-68-2 C36-52-1 CIR1-0G2-4G-1G-M-M CIR-2-7-3G-N-M-A27 D1-370-DB-020 D1-371-DA-088 D20-422-DB-074 D29-381-DA-091 D30-391-DA-003 D30-395-DB-054 D33-404-DB-007 D36-398-DB-083 D38-413-DB-095 D46-417-DB-022 D47-362-DA-094 D48-382-DB-055 D9-331-DA-029 DEKABES Papesek PCR-2-23C-1-M-1 SANTO-275 Wiley)

REF_DIR="/path/to/reference/"
DB="/path/to/output/SbicolorPHG_CHIBAS_05-04-2019_25taxa.db"
BAM_DIR="./Jensen_sorghumPHG_Sep2019/1_buildPHG/createHaplotypes/"
CONFIG_FILE="./p_sorghumphg/1_buildPHG/buildPHGFromDocker/input_files/config.txt"
INTERVALS="./p_sorghumphg/1_buildPHG/buildPHGFromDocker/input_files/Sbicolor_conserved_gene_intervals_extended_sorted_merged500.bed"

for TAXON in "${taxonList[@]}"
do

docker1 run --name cbsu_phg_container_${TAXON} --rm \
        -v ${REF_DIR}:/tempFileDir/data/reference/ \
        -v ${BAM_DIR}/${TAXON}/:/tempFileDir/data/bam/${TAXON}/DedupBAMs/ \
        -v ${INTERVALS}:/tempFileDir/data/bam/temp/intervals.bed \
        -v ${DB}:/tempFileDir/outputDir/SbicolorPHG_CHIBAS_05-04-2019_25taxa.db \
        -v ${CONFIG_FILE}:/tempFileDir/data/config.txt \
        -t maizegenetics/phg:0.0.9 \
        /CreateHaplotypesFromGVCF.groovy -config /tempFileDir/data/config.txt \
                        -t ${TAXON} \
                        -i /tempFileDir/data/bam/${TAXON}/DedupBAMs/${TAXON}_haplotype_caller_output.g.vcf.gz

done

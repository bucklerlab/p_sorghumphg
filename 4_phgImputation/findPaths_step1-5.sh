#!/bin/bash

config_path="/workdir/sej65/LoadRefDataDocker/config.txt"
consensus_level=(CONSENSUS_mxDiv00025_newtest)
database_fasta="24taxa_mxDiv00025_pangenome_fasta.fa"
minimap_index="24taxa_mxDiv00025_minimapIdx.mmi"
fastq_dir_prefix="/workdir/sej65/downsampled1000_rhAmpSeq"
log_prefixes="8-10_findPaths_downsampled1000_rhAmpSeq"

for level in "${consensus_level[@]}"
do

#mkdir ./${level}

#Step 1: pull out the fasta files (This step only needs to be run once per DB)
time ./tassel-5-standalone/run_pipeline.pl -Xmx120g -debug \
-HaplotypeGraphBuilderPlugin \
-configFile ${config_path} \
-methods ${level} \
-includeVariantContexts false \
-endPlugin \
-WriteFastaFromGraphPlugin \
-outputFile ./${level}_${database_fasta} \
-endPlugin > ${log_prefixes}_${level}_step1.log

#Step 2: Index the pangenome fasta using minimap2. Only need to run once per DB.
time minimap2/minimap2 -d ./${level}_${minimap_index} -k 21 -w 11 -I 120G ./${level}_${database_fasta} > ${log_prefixes}_${level}_step2.log

#Step 3: Run FastqDirToMappingPlugin
mapping_dir="./downsampled1000_rhAmpSeq_mappingdir"

mkdir ${mapping_dir}_${level}

./tassel-5-standalone/run_pipeline.pl -debug -Xmx120G \
-HaplotypeGraphBuilderPlugin \
-configFile ${config_path} \
-methods ${level} \
-includeVariantContexts false \
-includeSequences false \
-endPlugin \
-FastqDirToMappingPlugin \
-minimap2IndexFile ./${minimap_index} \
-fastqDir ${fastq_dir_prefix}/ \
-mappingFileDir ${mapping_dir}_${level}/ \
-paired true \
-endPlugin > ${log_prefixes}_step3_${level}.log

#Step 4: Find paths
position_vcf="/workdir/sej65/TERRA_240_renamedChr.vcf"
reference="/workdir/sej65/Reference/Sbicolor_313_v3.0_numberedChr.fa"
mkdir downsampled1000_rhAmpSeq_${level}_paths

./tassel-5-standalone/run_pipeline.pl -debug -Xmx120g \
-HaplotypeGraphBuilderPlugin \
-configFile ${config_path} \
-methods ${level},refRegionGroup \
-includeVariantContexts false \
-includeSequences false \
-endPlugin \
-HapCountBestPathToTextPlugin \
-configFile ${config_path} \
-inclusionFileDir ${mapping_dir}_${level}/ \
-taxa 00-SB-FSDT427,BF-95-11-195,C10-68-2,C36-52-1,CIR1-0G2-4G-1G-M-M,CIR-2-7-3G-N-M-A27,\
D1-370-DB-020,D1-371-DA-088,D20-422-DB-074,D29-381-DA-091,D30-391-DA-003,D30-395-DB-054,\
D33-404-DB-007,D36-398-DB-083,D38-413-DB-095,D46-417-DB-022,D47-362-DA-094,D48-382-DB-055,\
D9-331-DA-029,DEKABES,Papesek,PCR-2-23C-1-M-1,SANTO-275,Wiley \
-outputDir ./rhAmpSeq_${level}_paths/ \
-hapCountMethod ${level}_smDB_hapCounts \
-pMethod ${level}_smDB_paths \
-endPlugin > ${log_prefixes}_step4_${level}.log

#Step 5: Create VCF file with known SNPs
./tassel-5-standalone/run_pipeline.pl -debug -Xmx120g \
-HaplotypeGraphBuilderPlugin \
-configFile ${config_path} \
-methods ${level},refRegionGroup \
-includeVariantContexts true \
-endPlugin \
-ImportHaplotypePathFilePlugin \
-inputFileDirectory ./downsampled1000_rhAmpSeq_${level}_paths/ \
-endPlugin \
-PathsToVCFPlugin \
-ref ${reference} \
-positionVCF ${position_vcf} \
-outputAllSNPs true \
-outputFile CHIBAS_smallDB_${level}_hapMapSites.vcf \
-endPlugin > ${log_prefixes}_step5_${level}.log

# After creating VCF files with the PHG, convert all to Hapmap for analysis in R.
#./tassel-5-standalone/run_pipeline.pl -debug -Xmx120g \
#-importGuess CHIBAS_smallDB_${level}_hapMapSites.vcf \
#-export CHIBAS_smallDB_${level}_hapMapSites \
#-exportType Hapmap

done







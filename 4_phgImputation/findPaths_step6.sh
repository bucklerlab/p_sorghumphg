#!/bin/bash

taxaList=(00-SB-FSDT427 BF-95-11-195 C10-68-2 C36-52-1 CIR1-0G2-4G-1G-M-M CIR-2-7-3G-N-M-A27 D1-370-DB-020 D1-371-DA-088 D20-422-DB-074 D29-381-DA-091 D30-391-DA-003 D30-395-DB-054 D33-404-DB-007 D36-398-DB-083 D38-413-DB-095 D46-417-DB-022 D47-362-DA-094 D48-382-DB-055 D9-331-DA-029 DEKABES PCR-2-23C-1-M-1 SANTO-275 Papesek Wiley)
seed_num=(10 11 12)
cov_level=(0.01xCoverage 0.1xCoverage 1xCoverage fullCoverage)

for taxon in "${taxaList[@]}"
do

for seed in "${seed_num[@]}"

do

for coverage in "${cov_level[@]}"
do

echo ${taxon} ${seed}

mkdir ./${coverage}_errorfiles

time ./tassel-5-standalone/run_pipeline.pl -debug -Xmx120g \
-VerifyPathsPlugin \
-configFile /workdir/sej65/LoadRefDataDocker/config.txt \
-methods CONSENSUS_mxDiv00025_newtest \
-pathFile ./${coverage}_paths/${taxon}_seed${seed}_24333*reads_multimap.txt_path.txt \
-targetTaxon ${taxon} \
-errOutputFile ./${coverage}_errorfiles/${taxon}_${coverage}_${seed}_errorFile.txt \
-endPlugin > ./${coverage}_errorfiles/8-1_${taxon}_${coverage}_${seed}_error.log

done

done

done

#Use grep to pull out error rate from VerifyPathsPlugin log files for each coverage level
#for file in `ls 8-1*`
#do

#grep "ErrorRate:" ${file} >> 24taxa_0.0xCoverage_errorRates.txt

#done

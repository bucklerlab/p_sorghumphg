This directory contains all scripts needed to re-create a sorghum PHG and reproduce analyses for the sorghum PHG paper. Raw sequence files can be found on NCBI SRA with BioProject ID PRJNA575060. Processed sequence files and other large files needed to rerun the analyses can be found at CyVerse (doi: https://doi.org/10.25739/nccb-0k78). Software versions used in the original analysis are included in parenthesis.

# Directory descriptions and instructions: #

## 0_identifyReferenceRanges ##
Scripts and files needed to choose references ranges for the sorghum PHG based on conservation of CDS between sorghum, maize, setaria, brachypodium, and rice. This step requires blast (2.6.0) and bedtools (v2.28.0) command line tools.

### Contents: ###
* create\_ref_ranges.sh: script to identify reference ranges and write to bed file
* SbicolorChromosomeLengths.txt: chromosome lengths for the Sbicolor\_v3.0.fa reference genome
* Sbicolor\_conserved\_gene\_intervals\_extended\_sorted\_merged.bed: bed file with final set of intervals used for the sorghum PHG
* reference\_genomes: List of genome versions used to identify reference ranges. Put reference CDS file for for sorghum, maize, setaria, brachypodium, and rice in this directory. After running the create_ref_ranges.sh script the directory will contain reference genomes, NCBI BLAST databases for each reference genome, and a combined BLAST database with all genomes except sorghum.
* gff\_files: Sbicolor\_313_v3.1.gene.gff3 gff file from Phytozome and other intermediate subsets of the gff file produced by the create\_ref_ranges.sh script
* bed_files: intermediate bed files produced by the create\_ref_ranges.sh script

### To re-run: ###
1. Download reference genomes listed in reference\_genomes/ref_genome_versions.txt and put fasta files in reference\_genomes directory.
2. `cd ./O_identifyReferenceRanges`
3. `bash create_ref_ranges.sh` 

## 	1_buildPHG ##
Scripts needed to build a PHG for sorghum based on WGS data. This step requires bwa (0.7.17), samtools (1.9), Sentieon (sentieon-genomics-201704.01), Docker (1.12.5), and the PHG docker image (0.0.9). 

### Contents: ###
* processFastqs: files to process fastq sequences files to GVCFs
	- fastqToBamFiles.pl: takes input fastq files, aligns them to the reference genome, and outputs a sorted bam file. 
	- sentieon\_HapCaller.sh: takes sorted bam files and processes them through the standard Sentieon DNAseq pipeline to produce vcf files for each taxon. 
* buildPHGFromDocker: scripts to build a PHG database
	- loadGenomeIntervals.sh: uses the input bed file of reference ranges to create an initial database with reference ranges and reference genome sequence
	- createHaplotypes.sh: takes a list of input taxa names and adds sequence for each taxon to the reference range intervals of the PHG. Expects taxon sequence data in GVCF format. 
	- createConsensus.sh: takes an input practical haplotype graph and collapses haplotypes based on user-defined divergence level to create consensus haplotypes.
	- config.txt: config file with parameters for all three scripts
* chooseConsensus: scripts and files used to identify the best consensus level for the PHG database
	- sqlite\_extractHapIDs.sh: sqlite commands to create path files for every taxon in the founder PHG at a set of consensus levels
	- phg_writeVcfFromPath.sh: TASSEL command to write a VCF file for each path created by the `sqlite_extractHapIDs.sh` script
	- calcDiscordance.sh: VCFtools command to calculate discordance for each consensus level
	- getRefRangeSummary.sh: TASSEL command to produce summaries for each consensus level
	- consensus\_haplotype\_counts.csv: File with the number of consensus haplotypes at each consensus level in the database
	- ChibasFounders_mergedVCFs_diploid_hetsToMissing_rmIndels_mac3_maxMissing1_anchorsOnly_thin100kb_sitesInHapMap.recode.vcf.gz: Set of trusted WGS SNPs from the Sentieon pipeline to evalute discordance. Created by running the `calcDiscordance.sh` script
	- consensus_summaries: directory containing summary data from the founder PHG database at each consensus level. Created by running the `getRefRangeSummary.sh` script
	- consensus_discordance: directory containing discordance results from running VCFtools comparing PHG-imputed VCF to variants called in the sentieon pipeline
	- consensus_vcfs: directory containing VCF files with PHG-imputed SNPs for each founder taxon at each consensus level. These files are created by creating path files with the `sqlite_extractHapIDs.sh` script and running `phg_writeVcfFromPath.sh`

### To re-run: ###
1. Download raw sequence data from SRA (). Alternatively, use processed sequence data from CyVerse and skip to step 4. For steps 6 and later, download directories from Cyverse 1_buildPHG/chooseConsensus/ and put in bitbucket repository directory 1_buildPHG/chooseConsensus/.
2. Download Sbicolor_313_v3.0.fa reference genome from Phytozome, index with bwa (`bwa index Sbicolor_313_v3.0.fa`). Update paths and add indexed reference genome name in line 6 of the loadGenomeIntervals.sh script. Update the number of threads and fastq file extensions as needed in lines 7, 10, 19, and 20.
	- From within the fastq directory run `perl fastqToBamFiles.pl`
3. Update Sentieon licence, release, and location in the sentieon_HapCaller.sh script as needed (lines 3, 5, and 6). Update reference directory (line 8) and output directory (line 12). 
	- From within the directory with bam files run `bash sentieon_HapCaller.sh`
4. Load the PHG docker image from Docker Hub with `docker pull maizegenetics/phg:0.0.9`. If you are starting from scratch rather than replicating the sorghum PHG paper analysis use maizegenetics/phg:latest to get the most recent PHG release.
5. Using files in ./input_files/ and GVCFs created in steps 1-3 or downloaded from CyVerse, make a new docker database, add WGS variant calls, and create consensus haplotypes. Update file paths to reflect the local directory structure as needed.
	- `bash loadGenomeIntervals.sh`
	- `bash createHaplotypes.sh`
	- `bash createConsensus.sh` Change the consensus level in the config file and database method name to repeat on a common database.
6. (Optional) Update taxaList, database name, output file, and db_methods variables in `sqlite_extractHapIDs.sh`. The db_methods are added in the format (method_name1,method_id1 method_name2,method_id2).
	- `bash sqlite_extractHapIDs.sh`
7. 

## 2_downsampleSequence ##
Scripts and files to downsample WGS sorghum data from ~8x coverage to 1x, 0.1x, and 0.01x coverage, and rhAmpSeq data from >2000 loci to 500 or 1000 random loci. This step requires seqtk (1.2-r102-dirty), bwa (0.7.17), and samtools (1.9). 

### Contents ###
* downsampleWGS.sh: seqtk command to create down-sampled sequence files for each taxon at each read count and for each random seed. Expects all sequences to have the suffix ".fq" and be in the same directory
* downsample_rhAmpSeq.sh: script to randomly downsample rhAmpSeq loci 
* downsampled_rhAmpSeq_500sites.bed: bed file with 500 random intervals from the original rhAmpSeq data
* downsampled_rhAmpSeq_1000sites.bed: bed file with 1000 random intervals from the original rhAmpSeq data
* usda_Buckler_sorghum-rhAmpSeq_genotyped_target_loci.bed: bed file with full set of rhAmpSeq target loci

### To re-run: ###
1. Update file extensions as needed and move `downsampleWGS.sh` script into a directory with WGS files that need to be down-sampled.
	- `bash downsampleWGS.sh`
2. Move `downsample_rhAmpSeq.sh` into the directory with rhAmpSeq fastq files.
	- `bash downsample_rhAmpSeq.sh`

## 3_beagleImputation ##
Scripts to create reference and imputation panels for Beagle imputation at multiple sequence depths. Requires bcftools (1.9), Beagle (5.0), and TASSEL 5.0 (20181005).

### Contents ###
* mergeVCFs.sh: bcftools command to merge variant files into a single VCF 
* imputeGenotypes.sh: script to create an imputed reference panel and impute data for all levels of coverage
* reference\_SNPs: unimputed reference panel, which is also used for full-coverage imputation.
* imputed\_SNPs: unimputed and imputed VCF files, imputed Hapmap files

### To re-run: ###
1. Downsample sequence using `downsampleWGS.sh` script from the _2\_downsampleSequence_ directory
2. Make directory for reference genome and bwa index the reference
3. Use the `fastqToBamFiles.pl` and `sentieon_HapCaller.sh` scripts in the _1\_buildPHG_ directory to align downsampled fastqs to the reference genome and call variants. Run `mergeVCFs` to merge VCF files at each coverage level. 
4. Alternatively, copy the directories from CyVerse 3_beagleImputation/reference_SNPs and 3_beagleImputation/imputed_SNPs to 3_beagleImputation in the bitbucket repository. 
5. Run `imputeGenotypes.sh` to make a reference panel and impute missing SNPs for each coverage level.

## 4_phgImputation ##
findPaths scripts to impute SNPs and find haplotypes with the PHG from skim sequence or randomly across the genome. Requires Docker (1.12.5) and PHG image maizegenetics/phg:0.0.9.

### Contents ###
* findPaths\_step1-5.sh: findPaths steps to map input fastqs to the PHG pangenome, create path files for each individual, and output a VCF file of SNP calls
* findPaths\_step6.sh: findPaths script to evaluate haplotype calling accuracy for each taxon
* PHG\_smallDB\_randomPath\_rmNullHaps_path.txt: random path IDs for the founder PHG DB
* PHG\_largeDB\_randomPath\_rmNullHaps_path.txt: random path IDs for the diversity PHG DB
* ChibasFounders\_GBS\_mac3\_cr0.8.recode.hmp.txt: GBS truth SNP set for imputation accuracy comparisons
* mxDiv00025\_largeDB\_haplotypeErrors: error rates for calling haplotypes at each coverage level with the diversity PHG. Files are produced by `findPaths_step6.sh` script
* mxDiv00025\_largeDB\_snpCalls: PHG SNP calls from the diversity PHG at each coverage level. Files are produced by `findPaths_step1-5.sh` script
* mxDiv00025\_smallDB\_haplotypeErrors: error rates for calling haplotypes at each coverage level with the founder PHG. Files are produced by `findPaths_step6.sh` script
* mxDiv00025\_smallDB\_snpCalls: PHG SNP calls from the founder PHG at each coverage level. Files are produced by `findPaths_step1-5.sh` script

### To re-run: ###
1. Make sure the paths and parameters in the config file in _1\_buildPHG_ are correct.
2. Run `findPaths_step1-5.sh` using downsampled fastq files to get VCF files from the database for each sequence coverage. Uncomment the tassel command at the bottom of the file to convert VCFs to hapmap format.
3. Run `findPaths_step6.sh` to evaluate the haplotype calling error for each taxon. Use the grep command at the end of the file to pull error rate from the log files.
4. Alternatively, copy all directories from CyVerse in 4_phgImputation/mxDiv00025* to the bitbucket repository 4_phgImputation/. These will be used in later steps.

## 5_genomicPrediction ##
Files for evaluating genomic prediction accuracy with PHG, GBS, and rhAmpSeq SNPs and haplotypes. Requires R (3.3.6).

### Contents ###
* makeHaplotypeMatrix.R: R script to read in PHG and create a haplotype relationship matrix. Code to create a random path file for evaluating accuracy at a set of random haplotypes is included in this file
* Haiti_GS-BLUEs_R.R: R script to make BLUEs for phenotype data.
* Haiti\_GS\_BLUEs\_reformatted.csv: BLUEs from phenotype data for 7 traits
* PHG: directory containing PHG database and config file used in `makeHaplotypeMatrix.R`
* genotype\_data: directory with GBS, PHG, and rhAmpSeq files used in `6_figures/figure5.R`

### To re-run: ###
1. Create PHG database using previous steps or copy DB from CyVerse in 5_genomicPrediction/PHG/SbicolorPHG_CHIBAS_05-04-2019_25taxa.db. 
2. Run `makeHaplotypeMatrix.R` and `Haiti_GS-BLUEs_R.R` 
3. Copy genotype_data directory from CyVerse to make figures.

## 6_figures ##
R scripts to re-create figures from sorghum PHG paper. PDFs for all figures

### Contents ###
* figure3.R
* figure4.R
* figure5.R
* figure6.R
* figureSF1.R
* make_figures.R
* Figure3.pdf
* Figure4.pdf
* Figure5.pdf
* Figure6.pdf
* FigureSF1.pdf

### To re-run: ###
1. Adjust paths in `make_figures.R` as necessary
2. Run `make_figures.R` which will source all other scripts in the directory





#Steps for processing rhAmpSeq reads to downsample to only 500 loci.
#Original loci are in usda_Buckler_sorghum-rhAmpSeq_genotyped_target_loci.bed

#1. Randomly sample 500 loci from the original 2112 loci collected, remove "Chr" from chromosome names
shuf -n 500 usda_Buckler_sorghum-rhAmpSeq_genotyped_target_loci.bed > newdownsampled_500sites_usda_Buckler_sorghum-rhAmpSeq_genotyped_target_loci.bed
sed -i 's/Chr0//g' downsampled_500sites_usda_Buckler_sorghum-rhAmpSeq_genotyped_target_loci.bed
sed -i 's/Chr//g' downsampled_500sites_usda_Buckler_sorghum-rhAmpSeq_genotyped_target_loci.bed

#2. Align each file to the reference genome.
threads=22
for file in *R1.fastq
do
echo ${file}

taxon_name=`ls ${file} | cut -d_ -f1,2`

bwa mem -t ${threads} -R '@RG\tID:${taxon_name}\tSM:rhAmpSeq_downsampled' /workdir/sej65/Reference/Sbicolor_313_v3.0_numberedChr.fa ${taxon_name}_rhAmpSeq_R1.fastq ${taxon_name}_rhAmpSeq_R2.fastq > ${taxon_name}_aligned.sam

done

#3. Filter the .sam file for reads that hit the selected loci.
for file in `ls *.sam`
do
echo ${file}

#4. Extract reads from the original fastq files based on read ID to create downsampled rhAmpseq fastqs.
samtools view -L downsampled_500sites_usda_Buckler_sorghum-rhAmpSeq_genotyped_target_loci.bed -o downsampled_${file} ${file}

taxon=`ls ${file} | cut -d_ -f1`
awk '{print $1}' downsampled_${file} > ${taxon}_downsampled_readIDs.txt

fastq_names=`ls *R1.fastq.gz | cut -d_ -f1,2`
seqtk subseq ${fastq_names}_rhAmpSeq_R1.fastq.gz ${taxon}_downsampled_readIDs.txt > ${taxon}_downsampled_rhAmpSeq_R1.fastq.gz
seqtk subseq ${fastq_names}_rhAmpSeq_R2.fastq.gz ${taxon}_downsampled_readIDs.txt > ${taxon}_downsampled_rhAmpSeq_R2.fastq.gz

done

#5. Run the downsampled rhAmpSeq data through the PHG findPaths pipeline
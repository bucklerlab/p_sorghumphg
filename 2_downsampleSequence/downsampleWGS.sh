#!/bin/bash

###This script runs seqtk (found at https://github.com/lh3/seqtk) to down-sample sequence reads from whole genome sequence to a random subset of reads. Three seeds are used for downsampling, and either 24,333; 243,333; or 2,433,333 paired reads are pulled out of the original files. These read counts correspond to approximately 0.01x, 0.1x, and 1x coverage of the sorghum genome, respectively.
##Expects all original WGS fastq files to be in the same directory as this script and to have the suffix "R1.fq" or "R2.fq". Paired-end reads are expected to have the same prefix.
##Requirements: seqtk

filelist=`ls *R1.fq | cut -d_ -f1`
rand_seed=(10 11 12)
num_reads_to_keep=(24333 243333 2433333)

export PATH=/programs/seqtk:$PATH

for file in $filelist
do
echo ${file}

for seed in "${rand_seed[@]}"
do
echo ${seed}

for num_reads in "${num_reads_to_keep[@]}"
do
echo ${num_reads}

seqtk sample -s${seed} ${file}_R1.fq ${num_reads} > ${file}_seed${seed}_${num_reads}reads_R1.fastq
seqtk sample -s${seed} ${file}_R2.fq ${num_reads} > ${file}_seed${seed}_${num_reads}reads_R2.fastq

done

done

done
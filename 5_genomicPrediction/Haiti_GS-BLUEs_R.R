###############################################################
# Haiti GS: estimation of BLUEs in training set
###############################################################
#--------------------------------------------------------------
# Script parameters
#--------------------------------------------------------------
# Working directory
wd <- ifelse(Sys.info()["nodename"] == "RS-BTGR226DT", "/home/gr226/Documents", "/workdir/gr226")
dir.create(wd, showWarnings = FALSE)
setwd(wd)

# Input
pheno_file <- "Haiti_GS/data/BLUEs/PhenoGS_Haiti_Kansas.csv"

excluded_xp <- c("GS1", "GS2", "GS4")

# Output
out_dir <- "Haiti_GS/data/BLUEs"

#--------------------------------------------------------------
# Libraries
#--------------------------------------------------------------
library(lme4)
library(foreach)
library(reshape2)

#--------------------------------------------------------------
# Data
#--------------------------------------------------------------
# Input
pheno <- read.csv(pheno_file)

# Formatting
pheno <- pheno[! pheno$xp %in% excluded_xp, ]
pheno$xp <- factor(pheno$xp)

for (factor_variable in c("subpop", "rep")) {
  pheno[, factor_variable] <- factor(pheno[, factor_variable])
}

pheno$sowing_date <- as.Date(pheno$sowing_date, format="%d-%b-%y")

traits <- colnames(pheno)[sapply(pheno, is.numeric)]

# Visualization
tiff(paste(out_dir, "scatter_plots.tiff", sep="/"), height=3600, width=3600, res=300, compression="lzw")
plot(pheno[,traits])
dev.off()

tiff(paste(out_dir, "histograms.tiff", sep="/"), height=2000, width=2000, res=300, compression="lzw")
par(mfrow=c(3, 3))
for (trait in traits) {
  hist(pheno[, trait], main=trait, xlab="Phenotype value")
}
dev.off()

#--------------------------------------------------------------
# Genotype mean estimation
#--------------------------------------------------------------
# Long format
BLUE_DF <- foreach(xp=c("all", levels(pheno$xp)), .combine=rbind) %:%
  foreach(trait=traits, .combine=rbind) %do% {
    
    print(paste(xp, trait, sep=": "))
    
    # Fixed effects
    if (xp == "all") {
      f <- as.formula(paste(trait, "~ line - 1 + xp + (1|line:xp) + (1|xp:rep)" ))
      fit <- lmer(f, data=pheno)
    } else {
      f <- as.formula(paste(trait, "~ line - 1 + (1|rep)" ))
      fit <- lmer(f, data=pheno[pheno$xp == xp, ])
    }
    
    beta <- fixef(fit)
    genotype_means <- beta[grep("line", names(beta))]
    genotypes <- sub("line", "", names(genotype_means))
    
    return(
      data.frame(
        xp=xp,
        trait=trait,
        genotype=genotypes,
        mean=genotype_means
      )
    )
    
  }

# Wide format
BLUE_list <- sapply(levels(BLUE_DF$xp), function(xp) {
  acast(BLUE_DF[BLUE_DF$xp == xp, ], genotype ~ trait, value.var = "mean")
}, simplify=FALSE)

# Output
write.table(BLUE_DF, paste(out_dir, "BLUE.csv", sep="/"), sep=",", quote=FALSE, row.names=FALSE)
saveRDS(BLUE_DF, paste(out_dir, "BLUE.rds", sep="/"))
saveRDS(BLUE_list, paste(out_dir, "BLUE_list.rds", sep="/"))

#--------------------------------------------------------------
# Mixed model analysis
#--------------------------------------------------------------
MMA <- foreach(xp=c("all", levels(pheno$xp)), .combine=rbind) %:%
  foreach(trait=traits, .combine=rbind) %do% {
    
    print(paste(xp, trait, sep=": "))
    
    # Random effects
    if (xp == "all") {
      
      f <- as.formula(paste(trait, "~ (1|line) + xp + (1|line:xp) + (1|xp:rep)" ))
      fit <- lmer(f, data=pheno)
      
      tab_xp <- table(pheno$line, pheno$xp) > 0
      tab_rep <- table(pheno$line, pheno$xp:pheno$rep)
      
      n_VC <- setNames(
        c(1, max(rowSums(tab_xp)), max(rowSums(tab_rep))),
        c("line", "line:xp", "Residual"))
      
    } else {
      
      DF <- pheno[pheno$xp == xp, ]
      
      f <- as.formula(paste(trait, "~ (1|line) + (1|rep)" ))
      fit <- lmer(f, data=DF)
      
      tab_rep <- table(DF$line, DF$xp:DF$rep)
      n_VC <- setNames(c(1, max(rowSums(tab_rep))), c("line", "Residual"))
      
    }
    
    VC <- as.data.frame(VarCorr(fit, comp="Variance"))
    S2 <- setNames(VC$vcov, VC$grp)
    
    V_G <- S2["line"]
    V_P <- sum(S2[names(n_VC)]/n_VC)
    H2 <- V_G / sum(S2[names(n_VC)]/n_VC)
    
    # Fixed effects
    if (xp == "all") {
      f <- as.formula(paste(trait, "~ line - 1 + xp + (1|line:xp) + (1|xp:rep)" ))
      fit <- lmer(f, data=pheno)
    } else {
      f <- as.formula(paste(trait, "~ line - 1 + (1|rep)" ))
      fit <- lmer(f, data=pheno[pheno$xp == xp, ])
    }
    
    beta <- fixef(fit)
    genotype_means <- beta[grep("line", names(beta))]
    
    return(
      data.frame(
        xp=xp,
        trait=trait,
        mean=mean(genotype_means, na.rm=TRUE),
        min=min(genotype_means, na.rm=TRUE),
        max=max(genotype_means, na.rm=TRUE),
        V_G=V_G,
        V_P=V_P,
        H2=H2
      )
    )
    
    
  }

# Output
write.table(MMA, paste(out_dir, "MMA.csv", sep="/"), sep=",", quote=FALSE, row.names=FALSE)
saveRDS(MMA, paste(out_dir, "MMA.rds", sep="/"))

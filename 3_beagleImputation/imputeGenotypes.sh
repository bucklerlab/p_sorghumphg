# Beagle command to create the imputed reference panel and use it to impute SNPs for a set of unimputed multisample VCF files.

run_step=1
input_ref_panel="./reference_SNPs/CHIBAS_founders_mergedGVCFs_missingToRef_hetsToMissing_chroms.recode.vcf"
imputed_ref_panel="./reference_SNPs/CHIBAS_founders_mergedGVCFs_missingToRef_hetsToMissing_chroms_imputedRefPanel.vcf"

downsampled_file_prefix="./imputedSNPs/unimputed_VCFs/HaitiFounders_sentieonPipeline"
downsampled_file_suffix="_sorted_intersectFullVCF"
coverage=(0.01xCoverage 0.1xCoverage 1xCoverage)
seed=(10 11 12)

for cov in "${coverage[@]}"
do

for num in "${seed[@]}"
do

if [ ${run_step} -le 1 ]
	then
		echo "Imputing reference panel:"
		java -Xmx100g -jar /programs/beagle/beagle.jar \
		gt=${input_ref_panel} \
		out=${imputed_ref_panel} \
		ne=100 nthreads=22
fi

if [ ${run_step} -le 2 ]
	then
		echo "Imputing low-coverage VCFs:"
		java -Xmx100g -jar /programs/beagle/beagle.jar \
		gt=${downsampled_file_prefix}_${coverage}Seed${seed}_${downsampled_file_suffix}.vcf.gz \
		ref=${imputed_ref_panel} \
		out=${downsampled_file_prefix}_${coverage}Seed${seed}_${downsampled_file_suffix}_imputed \
		ne=100 nthreads=22
fi

done
done

if [${run_step} -le 3 ]
	then
		echo "Masking full-coverage genotypes:"
		full_coverage=`ls ${input_ref_panel} | cut -d. -f1,2`
		./tassel-5-standalone/run_pipeline.pl -Xmx100g -debug -MaskGenotypesPlugin \
		-inputFile ${input_ref_panel} \
		-outputFile ${full_coverage}_masked1pct.vcf -propSitesMask 0.01 -depthToMask 7 -endPlugin > maskGenotypes.log &

		echo "Imputing full-coverage genotypes:"
		java -Xmx100g -jar /programs/beagle/beagle.jar \
		gt=${full_coverage}_masked1pct.vcf \
		ref=${imputed_ref_panel} \
		out=${downsampled_file_prefix}_fullCoverage_masked1pct_imputed \
		ne=100 nthreads=22
fi

#The following commands can be uncommented to change the names of taxa in the imputed SNP files and more easily compare the imputed files to a trusted set of SNPs
# gunzip *.gz
# sed -i.original -E 's/_seed[0-9]{2}//g' ${downsampled_file_prefix}_${coverage}Seed${seed}_${downsampled_file_suffix}_imputed.vcf
# sed -i.original 's/_fullCoverage//g' ${downsampled_file_prefix}_fullCoverage_masked1pct_imputed.vcf


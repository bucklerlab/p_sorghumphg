# Use bcftools to merge VCF files output by Sentieon HapCaller pipeline
# Keep -0 flag if using this comand to create the VCF file that will make the reference panel, otherwise remove

for file in `ls *.vcf`
do
echo ${file}

bgzip ${file}
tabix -p vcf ${file}

done

threads=22
output_file="CHIBAS_founders_mergedGVCFs_missingToRef.vcf"
hapCaller_suffix="TruSeq_WGS_haplotype_caller_output.vcf.gz"


bcftools merge --threads ${threads} -0 -o ${output_file} 00-SB-FSDT427_${hapCaller_suffix} BF-95-11-195__${hapCaller_suffix} C10-68-2__${hapCaller_suffix} C36-52-1_${hapCaller_suffix} CIR-2-7-3G-N-M-A27_${hapCaller_suffix} CIR1-0G2-4G-1G-M-M_${hapCaller_suffix} D1-370-DB-020_${hapCaller_suffix} D1-371-DA-088_${hapCaller_suffix} D20-422-DB-074_${hapCaller_suffix} D29-381-DA-091_${hapCaller_suffix} D30-391-DA-003_${hapCaller_suffix} D30-395-DB-054_${hapCaller_suffix}  D33-404-DB-007_${hapCaller_suffix} D36-398-DB-083_${hapCaller_suffix} D38-413-DB-095_${hapCaller_suffix} D46-417-DB-022_${hapCaller_suffix} D47-362-DA-094_${hapCaller_suffix} D48-382-DB-055_${hapCaller_suffix} D9-331-DA-029_${hapCaller_suffix} DEKABES_${hapCaller_suffix} PCR-2-23C-1-M-1_${hapCaller_suffix} Papesek_${hapCaller_suffix} SANTO-275_${hapCaller_suffix} Wiley_${hapCaller_suffix}


############################################
# Supplemental figure 1: Genomic prediction accuracy for diversity PHG
# Main 'make_figures.R' script sets directories for all input SNP and haplotype GRM files
# Takes input SNP files and makes relationship matrix with SNPRelate, then uses rrBLUP to calculate prediction accuracies
# Assumption 1: all input files are filtered for MAF >= 0.05 and CR >= 0.8
# Assumption 2: the GBS data and phenotypes are already read into the .GlobalEnv when creating figure 6
# Script adapted from code by Guillaume Ramstein
############################################


library(data.table)
library(foreach)
library(rrBLUP)
library(gdsfmt)
library(SNPRelate)
library(tidyr)
library(ggplot2)


HRM <- readRDS(divPHG_hrm_path) 
rownames(HRM) <- gsub("_", "-", rownames(HRM))
colnames(HRM) <- gsub("_", "-", colnames(HRM))

snpgdsVCF2GDS(divPHG_VCF_path, "divPHG_snp.gds")
gds <- snpgdsOpen("divPHG_snp.gds")
grm <- snpgdsGRM(gds, method = "EIGMIX")
grm_mx <- grm$grm
rownames(grm_mx) <- gsub("_", "-", grm$sample.id)
colnames(grm_mx) <- gsub("_", "-", grm$sample.id)
snpgdsClose(gds)

GRM_lgphg_full <- grm_mx[-c(206), -c(206)] #Sample C1 is duplicated. Remove the second instance of it

# Cross-validation sets
output_lgDB <- data.frame()
for (i in 1:10) {
  set.seed(i)
  traits = c('Precocity', 'Height', 'Brix', 'JuiceWeight', 'LeafWeight', 'Yield', 'StemWeight')
  
  samples <- Reduce(intersect, list(rownames(GRM_GBS), rownames(HRM),
                                    rownames(phenotypes), rownames(GRM_lgphg_full)))
  phenotypes <- phenotypes[samples,]
  
  n <- length(samples)
  fold <- sample(rep(1:5, each=ceiling(n/5)))[1:n]
  
  genotypes = list('GRM_GBS', 'HRM', 'GRM_lgphg_full')
  
  for (geno in genotypes) {
    genofile <- get(geno)
    genofile <- genofile[samples, samples]
    
    correlations = expand.grid(trait=traits, k=1:5, r=NA, seed=1:10)
    
    for (cur_trait in traits) {
      y = as.numeric(phenotypes[, cur_trait])
      for (cvset in 1:5) {
        y_TS = y
        y_TS[fold == cvset] <- NA
        fit = mixed.solve(as.numeric(y_TS), K = as.matrix(genofile))
        y_hat = fit$u[samples[fold == cvset]]
        y_VS = y[fold == cvset]
        r <- cor(y_hat, y_VS, use = 'complete')
        
        output_lgDB <- rbind(output_lgDB, data.frame(Type=geno, Trait=cur_trait, k=cvset, r=r, seed = i))
      }
    }
  }
}










#!/bin/bash

###Script to identify and create a bed file with conserved reference ranges for the sorghum Practical Haplotype Graph. Adapted from method designed by Merritt Burch.
# Requires NCBI BLAST command line tools

##Step 1: Create BLAST databases with CDS for all species of interest. 
# The script here uses CDS from brachypodium, setaria, maize, and rice as reference databases for sorghum.
# Reference genomes for all 5 species can be downloaded from Phytozome at https://phytozome.jgi.doe.gov/pz/portal.html.
filelist="./reference_genomes/*.cds.*"
for file in $filelist; do echo "Reference: " $file; makeblastdb -in ${file} -input_type fasta -dbtype nucl -title ${file}_cds -out ${file}_cds.db; done

##Step 2: Combine all databases together
blastdb_aliastool -dblist "Bdistachyon_314_v3.1.cds.fa_cds.db Osativa_323_v7.0.cds.fa_cds.db Sitalica_312_v2.2.cds.fa_cds.db Zea_mays.B73_RefGen_v4.cds.all_changedIDs.fa_cds.db" -dbtype nucl -out ./reference_genomes/four-species_combined_blastdb -title "Four-species BLAST DB"

##Step 3: BLAST the CDS for sorghum against the combined database. Only output the top hit for each target
blastn -db ./reference_genomes/four-species_combined_blastdb -query ./reference_genomes/Sbicolor_313_v3.1.cds.fa -out ./gff_files/Sbicolor_conserved_genes.txt -outfmt 6 -max_target_seqs 1 -max_hsps 1

##Step 4: Subset the BLAST results to create a file with unique gene IDs. (Second command removes the ".X" that comes from the CDS identifier)
echo "Subsetting BLAST results..."
awk '{print $1}' ./gff_files/Sbicolor_conserved_genes.txt | sort -u > ./gff_files/Sbicolor_conserved_genes_geneID.txt
awk 'BEGIN { FS = "." } ; { print $1"."$2 }' ./gff_files/Sbicolor_conserved_genes_geneID.txt > ./gff_files/Sbicolor_conserved_genes_geneID2.txt

##Step 5: Subset the full gff file to exclude anything not listed as a gene, parse file to keep only the chromosome, gene start, gene end, and gene ID
echo "Selecting gene records..."
grep "gene" ./gff_files/Sbicolor_313_v3.1.gene.gff3 > ./gff_files/Sbicolor_313_v3.1_genesOnly.gff3

python << END
# -*- coding: utf-8 -*-
f = open("./gff_files/Sbicolor_313_v3.1_genesOnly.gff3", mode="r")
splitgff = [line.strip('\n').split('\t') for line in f]
f.close()
w = open("./gff_files/Sbicolor_313_v3.1_genesOnly_split.txt", "w")
#w.write("Chrom\tStart\tEnd\tGeneID\n")
for line in splitgff:
	w.write(line[0] + "\t" + line[3] + "\t" + line[4] + "\t" + line[8].split("=")[1].split(".v3")[0] + "\n")
w.close()
END

##Step 6: Merge the conserved gene file with the gene-only gff file, keeping all genes with an ID in the unique list of BLAST hits. This is the core set of genes to use with the PHG
echo "Merging conserved genes with gff file..." 
#Uncomment the command below to maintain the gene ID in the new file
#awk 'FNR==NR { a[$4]=$1; next } { print $1"\t"$2"\t"$3"\t"$4a[$4] }' ./gff_files/Sbicolor_conserved_genes_geneID2.txt ./gff_files/Sbicolor_313_v3.1_genesOnly_split.txt > ./gff_files/Sbicolor_conserved_gene_intervals_wID.bed

#To remove the gene ID in the new file
awk 'FNR==NR { a[$4]=$1; next } { print $1"\t"$2"\t"$3a[$4] }' ./gff_files/Sbicolor_conserved_genes_geneID2.txt ./gff_files/Sbicolor_313_v3.1_genesOnly_split.txt > ./bed_files/Sbicolor_conserved_gene_intervals.bed

##Step 7: Remove non-chromosomal contigs - lines that start with 'super'. Extend the gene intervals 1kb on either side of the listed coordinates and merge any intervals that overlap or are less than 500bp apart. 
#Note: bedtools slop requires a list of chromosome lengths so that it doesn't extend past the ends of the chromosome. Bedtools merge requires a sorted bedfile. Although the file should be close to sorted already, the sort step is included to sort first on column 1, then on column 2.
echo "Extending gene intervals and merging overlapping records..."
grep -v "super" ./bed_files/Sbicolor_conserved_gene_intervals.bed > ./bed_files/Sbicolor_conserved_gene_intervals_rmSuper.bed
bedtools slop -i ./bed_files/Sbicolor_conserved_gene_intervals_rmSuper.bed -g ./SbicolorChromosomeLengths.txt -b 1000 > ./bed_files/Sbicolor_conserved_gene_intervals_extended.bed
sort -k 1,1n -k2,2n ./bed_files/Sbicolor_conserved_gene_intervals_extended.bed > ./bed_files/Sbicolor_conserved_gene_intervals_extended_sorted.bed
bedtools merge -i ./bed_files/Sbicolor_conserved_gene_intervals_extended_sorted.bed -d 500 > ./Sbicolor_conserved_gene_intervals_extended_sorted_merged.bed

echo "Finished!"
#For sorghum v3.1, chromosome lengths are as follows:
#1	80884392
#2	77742459
#3	74386277
#4	68658214
#5	71854669
#6	61277060
#7	65505356
#8	62686529
#9	59416394
#10	61233695


